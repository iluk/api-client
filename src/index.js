import es6Promise from 'es6-promise';
es6Promise.polyfill()

import esc from 'jquery-param';

const LOCALSTORAGE_KEY = 'iluk-api-client';

class iLUKAPIClient {

  // I'm passing in fetch here so that I
  // can run this as a node test as well.
  constructor(baseUrl) {
    this.base = baseUrl;
    this.token = null;
    this.events = {};
    this.ready = this.restore();
    this.EVENTS = {
      SESSION_EXPIRED: 'SESSION_EXPIRED',
      READY: 'READY'
    }

    this.users = this._resource({
      route: 'users'
    })

    this.vendors = this._resource({
      route: 'vendors'
    })

    this.employees = this._resource({
      route: 'employees'
    })

    this.appointmentCategories = this._resource({
      route: 'appointment_categories'
    })

    this.appointmentTypes = this._resource({
      route: 'appointment_types'
    })

    this.employeeAppointmentTypes = this._resource({
      route: 'employee_appointment_types'
    })

    this.availabilities = this._resource({
      route: 'availabilities',
      lists: [
        { routeName: 'search', method: 'get' }
      ]
    })

    this.profiles = this._resource({
      route: 'profiles',
      details: [
        { routeName: 'confirm', method: 'get' }
      ]
    })

    this.bookings = this._resource({
      route: 'bookings'
    })

    this.traditional_bookings = this._resource({
      route: 'traditional_bookings'
    })

    this.meta = this._resource({
      route: 'meta'
    })

    this.payouts = this._resource({
      route: 'payouts'
    })

    this.me = this.users.get.bind(this, 'me')
  }

  // this is a silly wrapper around localStorage so we can
  // work with asyncstorage in react native as well.
  // need to return a promise.
  _getItem(key) {
    return new Promise((resolve, reject) => {
      var item = localStorage.getItem(key)
      if (item && item.then) {
        item.then(resolve)
      } else {
        resolve(item)
      }
    })
  }

  _setItem(key, value) {
    return new Promise((resolve, reject) => {
      var res = localStorage.setItem(key, value)
      if (res && res.then) {
        res.then(resolve)
      } else {
        resolve(res)
      }
    })
  }

  save() {
    return new Promise((resolve, reject) => {
      this._setItem(LOCALSTORAGE_KEY, JSON.stringify({
        token: this.token
      })).then(resolve)
    })
  }

  restore() {
    return new Promise((resolve, reject) => {
      this._getItem(LOCALSTORAGE_KEY).then((serialized) => {
        var stored;
        if (serialized) {
          stored = JSON.parse(serialized);
          if (stored.token) {
            this.token = stored.token;
          }
        }
        resolve()
      })
    })
  }

  fetch(path, params) {
    var url = `${this.base}${path}`;
    var qsPos = url.indexOf('?')

    if (qsPos > -1) {
      if (url.substr(qsPos - 1, 1) !== '/')
        url = url.split('?').join('/?');
    } else {
      if (url.substr(url.length - 1) !== '/')
        url += '/';
    }

    if (!params.headers)
      params.headers = {};

    if (this.token)
      params.headers.authorization = `Token ${this.token}`;

    params.headers['Content-Type'] = 'application/json';
    params.headers.Accept = 'application/json';
    return fetch(url, params);
  }

  logIn(username, password) {
    return new Promise((resolve, reject) => {
      this.fetch('/api/token_auth', {
        method: 'post',
        body: JSON.stringify({
          username,
          password
        })
      }).then((res) => {
        if (res.status !== 200)
          reject(res);

        return res.json();
      }).then((body) => {
        this.token = body.token;
        this.save();
        resolve();
      }).catch(err => reject(err));
    });
  }

  logInFacebook(token, email) {
    return new Promise((resolve, reject) => {
      this.fetch('/api/facebook', {
        method: 'post',
        body: JSON.stringify({
          token: token,
          email: email
        })
      }).then((res) => {
        if (res.status === 204)
          reject(204);
        else if (res.status !== 200)
          reject(res);
        else
          return res.json();
      }).then((body) => {
        this.token = body.token;
        this.save();
        resolve();
      }).catch(err => reject(err));
    });
  }

  createAccount(data) {
    return new Promise((resolve, reject) => {
      this.fetch('/api/profiles', {
        method: 'post',
        body: JSON.stringify(data)
      }).then((res) => {
        if (res.status !== 200)
          reject(res);

        return res.json();
      }).then((body) => {
        this.token = body.token;
        this.save();
        resolve();
      }).catch(err => reject(err));
    })
  }

  logOut() {
    return new Promise((resolve, reject) => {
      if (this.token) {
        this.fetch('/api/logout', {
          method: 'post'
        }).then((res) => {
          this._destroySession();
          if (res.status !== 200) {
            reject(res.json());
          } else {
            resolve(res.json());
          }
        }).catch(err => reject(err));
      } else {
        reject();
      }
    });
  }

  addEventListener(e, hook) {
    if (!this.events[e])
      this.events[e] = []

    this.events[e].push(hook)
  }

  _triggerEvent(e) {
    if (this.events[e]) {
      this.events[e].forEach((hook) => {
        hook()
      })
    }
  }

  _destroySession() {
    this.token = null;
    this.save();
  }

  _resource(config) {
    var baseUrl = `/api/${config.route}/`;
    var self = this;

    var encodeParams = function (params) {
      var query;

      if (params) {
        query = '?' + Object
          .keys(params)
          .map(key => {
            let arr = []
            if (params[key] instanceof Array) {
              arr = params[key]
            } else {
              arr = [params[key]]
            }

            return arr.map((val) => {
              return `${encodeURIComponent(key)}=${encodeURIComponent(val)}`
            }).join('&')
          })
          .join('&')
      } else {
        query = '';
      }

      return query;
    }

    var checkRes = function (res) {
      return new Promise((resolve, reject) => {
        switch (res.status) {

          case 401:
            self._destroySession();
            self._triggerEvent(self.EVENTS.SESSION_EXPIRED)
            return reject("Session expired")

          case 400:
            return res.json().then((body) => reject(body))

          default:
            return resolve(res)
        }
      })
    };

    var query = function (params) {
      var url
      var qs = encodeParams(params)
      url = `${baseUrl}${qs}`
      return new Promise((resolve, reject) => {
        self.fetch(url, {
          method: 'get'
        }).then((res) => {
          return checkRes(res)
        }).then((res) => {
          resolve(res.json())
        }).catch(err => reject(err))
      });
    }

    var get = function (id, params) {
      var qs = encodeParams(params);
      var url = `${baseUrl}${id}/${qs}`;
      return new Promise((resolve, reject) => {
        self.fetch(url, {
          method: 'get'
        }).then((res) => {
          return checkRes(res)
        }).then((res) => {
          resolve(res.json())
        }).catch(err => reject(err))
      })
    }

    var update = function (id, data) {
      var url = `${baseUrl}${id}/`;
      return new Promise((resolve, reject) => {
        self.fetch(url, {
          method: 'put',
          body: JSON.stringify(data)
        }).then((res) => {
          return checkRes(res)
        }).then((res) => {
          resolve(res.json())
        }).catch(err => reject(err))
      })
    }

    var create = function (data) {
      var url = `${baseUrl}`;
      return new Promise((resolve, reject) => {
        self.fetch(url, {
          method: 'post',
          body: JSON.stringify(data)
        }).then((res) => {
          return checkRes(res)
        }).then((res) => {
          resolve(res.json())
        }).catch(err => reject(err))
      })
    }

    var del = function (id) {
      var url = `${baseUrl}${id}/`;
      return new Promise((resolve, reject) => {
        self.fetch(url, {
          method: 'delete'
        }).then((res) => {
          return checkRes(res)
        }).then((res) => {
          resolve()
        }).catch(err => reject(err))
      })
    }

    var methods = {
      query: query,
      get: get,
      update: update,
      create: create,
      delete: del
    }

    if (config.details) {
      config.details.forEach((detail) => {
        methods[detail.routeName] = function (id, data) {
          let url = `${baseUrl}${id}/${detail.routeName}/`
          let params = {
            method: detail.method
          }

          if (detail.method === 'get') {
            url += encodeParams(data)
          }

          return new Promise((resolve, reject) => {
            self.fetch(url, params).then((res) => {
              return checkRes(res)
            }).then((res) => {
              resolve(res.json())
            }).catch(err => reject(err))
          })
        }
      })
    }

    if (config.lists) {
      config.lists.forEach((list) => {
        methods[list.routeName] = function (data) {
          let url = `${baseUrl}${list.routeName}/`
          let params = {
            method: list.method
          }

          if (list.method === 'get') {
            url += encodeParams(data)
          }

          return new Promise((resolve, reject) => {
            self.fetch(url, params).then((res) => {
              return checkRes(res)
            }).then((res) => {
              resolve(res.json())
            }).catch(err => reject(err))
          })
        }
      })
    }

    return methods
  }
}

export default iLUKAPIClient;

export { LOCALSTORAGE_KEY }
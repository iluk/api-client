'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LOCALSTORAGE_KEY = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _es6Promise = require('es6-promise');

var _es6Promise2 = _interopRequireDefault(_es6Promise);

var _jqueryParam = require('jquery-param');

var _jqueryParam2 = _interopRequireDefault(_jqueryParam);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

_es6Promise2.default.polyfill();

var LOCALSTORAGE_KEY = 'iluk-api-client';

var iLUKAPIClient = function () {

  // I'm passing in fetch here so that I
  // can run this as a node test as well.
  function iLUKAPIClient(baseUrl) {
    _classCallCheck(this, iLUKAPIClient);

    this.base = baseUrl;
    this.token = null;
    this.events = {};
    this.ready = this.restore();
    this.EVENTS = {
      SESSION_EXPIRED: 'SESSION_EXPIRED',
      READY: 'READY'
    };

    this.users = this._resource({
      route: 'users'
    });

    this.vendors = this._resource({
      route: 'vendors'
    });

    this.employees = this._resource({
      route: 'employees'
    });

    this.appointmentCategories = this._resource({
      route: 'appointment_categories'
    });

    this.appointmentTypes = this._resource({
      route: 'appointment_types'
    });

    this.employeeAppointmentTypes = this._resource({
      route: 'employee_appointment_types'
    });

    this.availabilities = this._resource({
      route: 'availabilities',
      lists: [{ routeName: 'search', method: 'get' }]
    });

    this.profiles = this._resource({
      route: 'profiles',
      details: [{ routeName: 'confirm', method: 'get' }]
    });

    this.bookings = this._resource({
      route: 'bookings'
    });

    this.traditional_bookings = this._resource({
      route: 'traditional_bookings'
    });

    this.meta = this._resource({
      route: 'meta'
    });

    this.payouts = this._resource({
      route: 'payouts'
    });

    this.me = this.users.get.bind(this, 'me');
  }

  // this is a silly wrapper around localStorage so we can
  // work with asyncstorage in react native as well.
  // need to return a promise.


  _createClass(iLUKAPIClient, [{
    key: '_getItem',
    value: function _getItem(key) {
      return new Promise(function (resolve, reject) {
        var item = localStorage.getItem(key);
        if (item && item.then) {
          item.then(resolve);
        } else {
          resolve(item);
        }
      });
    }
  }, {
    key: '_setItem',
    value: function _setItem(key, value) {
      return new Promise(function (resolve, reject) {
        var res = localStorage.setItem(key, value);
        if (res && res.then) {
          res.then(resolve);
        } else {
          resolve(res);
        }
      });
    }
  }, {
    key: 'save',
    value: function save() {
      var _this = this;

      return new Promise(function (resolve, reject) {
        _this._setItem(LOCALSTORAGE_KEY, JSON.stringify({
          token: _this.token
        })).then(resolve);
      });
    }
  }, {
    key: 'restore',
    value: function restore() {
      var _this2 = this;

      return new Promise(function (resolve, reject) {
        _this2._getItem(LOCALSTORAGE_KEY).then(function (serialized) {
          var stored;
          if (serialized) {
            stored = JSON.parse(serialized);
            if (stored.token) {
              _this2.token = stored.token;
            }
          }
          resolve();
        });
      });
    }
  }, {
    key: 'fetch',
    value: function (_fetch) {
      function fetch(_x, _x2) {
        return _fetch.apply(this, arguments);
      }

      fetch.toString = function () {
        return _fetch.toString();
      };

      return fetch;
    }(function (path, params) {
      var url = '' + this.base + path;
      var qsPos = url.indexOf('?');

      if (qsPos > -1) {
        if (url.substr(qsPos - 1, 1) !== '/') url = url.split('?').join('/?');
      } else {
        if (url.substr(url.length - 1) !== '/') url += '/';
      }

      if (!params.headers) params.headers = {};

      if (this.token) params.headers.authorization = 'Token ' + this.token;

      params.headers['Content-Type'] = 'application/json';
      params.headers.Accept = 'application/json';
      return fetch(url, params);
    })
  }, {
    key: 'logIn',
    value: function logIn(username, password) {
      var _this3 = this;

      return new Promise(function (resolve, reject) {
        _this3.fetch('/api/token_auth', {
          method: 'post',
          body: JSON.stringify({
            username: username,
            password: password
          })
        }).then(function (res) {
          if (res.status !== 200) reject(res);

          return res.json();
        }).then(function (body) {
          _this3.token = body.token;
          _this3.save();
          resolve();
        }).catch(function (err) {
          return reject(err);
        });
      });
    }
  }, {
    key: 'logInFacebook',
    value: function logInFacebook(token, email) {
      var _this4 = this;

      return new Promise(function (resolve, reject) {
        _this4.fetch('/api/facebook', {
          method: 'post',
          body: JSON.stringify({
            token: token,
            email: email
          })
        }).then(function (res) {
          if (res.status === 204) reject(204);else if (res.status !== 200) reject(res);else return res.json();
        }).then(function (body) {
          _this4.token = body.token;
          _this4.save();
          resolve();
        }).catch(function (err) {
          return reject(err);
        });
      });
    }
  }, {
    key: 'createAccount',
    value: function createAccount(data) {
      var _this5 = this;

      return new Promise(function (resolve, reject) {
        _this5.fetch('/api/profiles', {
          method: 'post',
          body: JSON.stringify(data)
        }).then(function (res) {
          if (res.status !== 200) reject(res);

          return res.json();
        }).then(function (body) {
          _this5.token = body.token;
          _this5.save();
          resolve();
        }).catch(function (err) {
          return reject(err);
        });
      });
    }
  }, {
    key: 'logOut',
    value: function logOut() {
      var _this6 = this;

      return new Promise(function (resolve, reject) {
        if (_this6.token) {
          _this6.fetch('/api/logout', {
            method: 'post'
          }).then(function (res) {
            _this6._destroySession();
            if (res.status !== 200) {
              reject(res.json());
            } else {
              resolve(res.json());
            }
          }).catch(function (err) {
            return reject(err);
          });
        } else {
          reject();
        }
      });
    }
  }, {
    key: 'addEventListener',
    value: function addEventListener(e, hook) {
      if (!this.events[e]) this.events[e] = [];

      this.events[e].push(hook);
    }
  }, {
    key: '_triggerEvent',
    value: function _triggerEvent(e) {
      if (this.events[e]) {
        this.events[e].forEach(function (hook) {
          hook();
        });
      }
    }
  }, {
    key: '_destroySession',
    value: function _destroySession() {
      this.token = null;
      this.save();
    }
  }, {
    key: '_resource',
    value: function _resource(config) {
      var baseUrl = '/api/' + config.route + '/';
      var self = this;

      var encodeParams = function encodeParams(params) {
        var query;

        if (params) {
          query = '?' + Object.keys(params).map(function (key) {
            var arr = [];
            if (params[key] instanceof Array) {
              arr = params[key];
            } else {
              arr = [params[key]];
            }

            return arr.map(function (val) {
              return encodeURIComponent(key) + '=' + encodeURIComponent(val);
            }).join('&');
          }).join('&');
        } else {
          query = '';
        }

        return query;
      };

      var checkRes = function checkRes(res) {
        return new Promise(function (resolve, reject) {
          switch (res.status) {

            case 401:
              self._destroySession();
              self._triggerEvent(self.EVENTS.SESSION_EXPIRED);
              return reject("Session expired");

            case 400:
              return res.json().then(function (body) {
                return reject(body);
              });

            default:
              return resolve(res);
          }
        });
      };

      var query = function query(params) {
        var url;
        var qs = encodeParams(params);
        url = '' + baseUrl + qs;
        return new Promise(function (resolve, reject) {
          self.fetch(url, {
            method: 'get'
          }).then(function (res) {
            return checkRes(res);
          }).then(function (res) {
            resolve(res.json());
          }).catch(function (err) {
            return reject(err);
          });
        });
      };

      var get = function get(id, params) {
        var qs = encodeParams(params);
        var url = '' + baseUrl + id + '/' + qs;
        return new Promise(function (resolve, reject) {
          self.fetch(url, {
            method: 'get'
          }).then(function (res) {
            return checkRes(res);
          }).then(function (res) {
            resolve(res.json());
          }).catch(function (err) {
            return reject(err);
          });
        });
      };

      var update = function update(id, data) {
        var url = '' + baseUrl + id + '/';
        return new Promise(function (resolve, reject) {
          self.fetch(url, {
            method: 'put',
            body: JSON.stringify(data)
          }).then(function (res) {
            return checkRes(res);
          }).then(function (res) {
            resolve(res.json());
          }).catch(function (err) {
            return reject(err);
          });
        });
      };

      var create = function create(data) {
        var url = '' + baseUrl;
        return new Promise(function (resolve, reject) {
          self.fetch(url, {
            method: 'post',
            body: JSON.stringify(data)
          }).then(function (res) {
            return checkRes(res);
          }).then(function (res) {
            resolve(res.json());
          }).catch(function (err) {
            return reject(err);
          });
        });
      };

      var del = function del(id) {
        var url = '' + baseUrl + id + '/';
        return new Promise(function (resolve, reject) {
          self.fetch(url, {
            method: 'delete'
          }).then(function (res) {
            return checkRes(res);
          }).then(function (res) {
            resolve();
          }).catch(function (err) {
            return reject(err);
          });
        });
      };

      var methods = {
        query: query,
        get: get,
        update: update,
        create: create,
        delete: del
      };

      if (config.details) {
        config.details.forEach(function (detail) {
          methods[detail.routeName] = function (id, data) {
            var url = '' + baseUrl + id + '/' + detail.routeName + '/';
            var params = {
              method: detail.method
            };

            if (detail.method === 'get') {
              url += encodeParams(data);
            }

            return new Promise(function (resolve, reject) {
              self.fetch(url, params).then(function (res) {
                return checkRes(res);
              }).then(function (res) {
                resolve(res.json());
              }).catch(function (err) {
                return reject(err);
              });
            });
          };
        });
      }

      if (config.lists) {
        config.lists.forEach(function (list) {
          methods[list.routeName] = function (data) {
            var url = '' + baseUrl + list.routeName + '/';
            var params = {
              method: list.method
            };

            if (list.method === 'get') {
              url += encodeParams(data);
            }

            return new Promise(function (resolve, reject) {
              self.fetch(url, params).then(function (res) {
                return checkRes(res);
              }).then(function (res) {
                resolve(res.json());
              }).catch(function (err) {
                return reject(err);
              });
            });
          };
        });
      }

      return methods;
    }
  }]);

  return iLUKAPIClient;
}();

exports.default = iLUKAPIClient;
exports.LOCALSTORAGE_KEY = LOCALSTORAGE_KEY;

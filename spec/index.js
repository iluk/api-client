import assert from 'assert';
import iLUKAPIClient from '../src';
import nock from 'nock';
import {LocalStorage} from 'node-localstorage';
import {LOCALSTORAGE_KEY} from '../src';

var BASE = 'http://localhost:8000';
global.localStorage = new LocalStorage('./.tmp');

describe('api', () => {
  var client;

  beforeEach(() => {
    client = new iLUKAPIClient(BASE);
  });

  after(() => {
    localStorage._deleteLocation();
  });

  describe('auth', () => {

    it('should handle failed login', () => {
      nock(BASE)
        .post('/api/token_auth/')
        .reply(400);

      return client.logIn('admin', 'testpass').then(function () {
        throw "This should have failed";
      }).catch(function (err) {
        var stored = JSON.parse(localStorage.getItem(LOCALSTORAGE_KEY));
        assert.equal(err.status, 400);
        assert.equal(client.token, null);
        assert.equal(stored, null);
      });
    })

    it('should retrieve token', () => {
      nock(BASE)
        .post('/api/token_auth/')
        .reply(200, {
          token: 'test'
        });

      return client.logIn('admin', 'testpass').then(function () {
        var stored = JSON.parse(localStorage.getItem(LOCALSTORAGE_KEY));
        assert.equal(client.token, 'test');
        assert.equal(stored.token, 'test');
      });
    });

    it('should log user out', () => {
      nock(BASE)
        .post('/api/logout/')
        .reply(200, {});

      client.token = 'test';

      localStorage.setItem(LOCALSTORAGE_KEY, JSON.stringify({
        token: 'test'
      }));

      return client.logOut().then(function () {
        var stored = JSON.parse(localStorage.getItem(LOCALSTORAGE_KEY));
        assert.equal(client.token, null);
        assert.equal(stored.token, null);
      });
    });

    it('should load stored configuration', (done) => {
      localStorage.setItem(LOCALSTORAGE_KEY, JSON.stringify({
        token: 'ridiculous',
      }));

      client = new iLUKAPIClient(BASE);
      client.ready.then(() => {
        assert.equal(client.token, 'ridiculous');
        done()
      })
    });

    it('should send token with authorized requests', () => {
      client.token = 'test';
    
      nock(BASE)
        .post('/api/logout/')
        .reply(200, function (req, body) {
          assert.equal(this.req.headers.authorization[0], 'Token test');
          return '{}';
        });

      return client.logOut();
    });

    it('should handle authorized request without token', () => {
      nock(BASE)
        .post('/api/logout/')
        .reply(401, {
          'detail': ''
        });

      return client.logOut().then((res) => {
        assert.fail('this should fail..');
      }).catch((err) => {
      });
    });

    it('should handle expired token', () => {
      var destroyHookCalled = false
      client.token = 'test';
      client.addEventListener(client.EVENTS.SESSION_EXPIRED, () => {
        destroyHookCalled = true
      })

      nock(BASE)
        .post('/api/logout/')
        .reply(401, {
          'detail': ''
        });

      return client.logOut().then((res) => {
        assert.fail('this should fail..');
      }).catch((err) => {
        assert.equal(client.token, null);
        assert.equal(destroyHookCalled, false)
      });
    });
  });

  describe('resources', () => {
    var resource;

    beforeEach(() => {
      resource = client._resource({
        route: 'test'
      });
    });

    describe('query', () => {
      it('should handle expired token', () => {
        var test = false
        client.token = 'test';
        client.addEventListener(client.EVENTS.SESSION_EXPIRED, () => {
          test = true
        })

        nock(BASE)
          .get('/api/test/')
          .reply(401, []);

        return resource.query().then((res) => {
          assert.fail('This should fail');
        }).catch((err) => {
          assert.equal(test, true)
          assert.equal(client.token, null);
        });
      });

      it('should work', () => {
        var testResponse = [{'test': 'test'}];
        nock(BASE)
          .get('/api/test/')
          .reply(200, testResponse);

        return resource.query().then((res) => {
          assert.deepEqual(res, testResponse);
        });
      });

      it('should work with params', () => {
        var testResponse = [{'test': 'test'}];
        var params = {expand: 'test.test', foo: 'bar'};

        nock(BASE)
          .get('/api/test/')
          .query(params)
          .reply(200, testResponse);

        return resource.query(params).then((res) => {
          assert.deepEqual(res, testResponse);
        });
      });
    });

    describe('get', () => {
      it('should handle expired token', () => {
        var test = false
        client.token = 'test';
        client.addEventListener(client.EVENTS.SESSION_EXPIRED, () => {
          test = true
        })

        nock(BASE)
          .get('/api/test/1/')
          .reply(401, {});

        return resource.get(1).then((res) => {
          assert.fail('This should fail');
        }).catch((err) => {
          assert.equal(client.token, null);
          assert.equal(test, true)
        });
      });

      it('should get', () => {
        var testResponse = {
          'test': 'test'
        }

        nock(BASE)
          .get('/api/test/1/')
          .reply(200, testResponse)

        return resource.get(1).then((res) => {
          assert.deepEqual(res, testResponse)
        })
      })

      it('should get with params', () => {
        var testResponse = {
          'test': 'test'
        }

        var params = {
          'expand': 'expanded',
          'foo': 'bar'
        }

        nock(BASE)
          .get('/api/test/1/')
          .query(params)
          .reply(200, testResponse)

        return resource.get(1, params).then((res) => {
          assert.deepEqual(res, testResponse)
        })
      })
    });

    describe('update', () => {
      it('should handle expired token', () => {
        var test = false
        client.token = 'test';
        client.addEventListener(client.EVENTS.SESSION_EXPIRED, () => {
          test = true
        })

        nock(BASE)
          .put('/api/test/1/')
          .reply(401, {});

        return resource.update(1).then((res) => {
          assert.fail('This should fail');
        }).catch((err) => {
          assert.equal(test, true)
          assert.equal(client.token, null);
        });
      });

      it('should update', () => {
        var testResponse = {
          'test': 'test'
        }

        nock(BASE)
          .put('/api/test/1/', testResponse)
          .reply(200, testResponse)

        return resource.update(1, testResponse).then((res) => {
          assert.deepEqual(res, testResponse)
        })
      })
    });

    describe('create', () => {
      it('should handle expired token', () => {
        var test = false
        client.token = 'test';
        client.addEventListener(client.EVENTS.SESSION_EXPIRED, () => {
          test = true
        })

        nock(BASE)
          .post('/api/test/')
          .reply(401, {});

        return resource.create({}).then((res) => {
          assert.fail('This should fail');
        }).catch((err) => {
          assert.equal(client.token, null);
          assert.equal(test, true)
        });
      });

      it('should create', () => {
        var testResponse = {
          'test': 'test'
        }

        nock(BASE)
          .post('/api/test/', testResponse)
          .reply(200, testResponse)

        return resource.create(testResponse).then((res) => {
          assert.deepEqual(res, testResponse)
        })
      })
    });

    describe('delete', () => {
      it('should handle expired token', () => {
        var test = false
        client.token = 'test';
        client.addEventListener(client.EVENTS.SESSION_EXPIRED, () => {
          test = true
        })

        nock(BASE)
          .delete('/api/test/1/')
          .reply(401, {});

        return resource.delete(1).then((res) => {
          assert.fail('This should fail');
        }).catch((err) => {
          assert.equal(test, true)
          assert.equal(client.token, null);
        });
      });

      it('should delete', () => {
        nock(BASE)
          .delete('/api/test/1/')
          .reply(204)

        return resource.delete(1).then((res) => {
          assert.deepEqual(res, undefined)
        })
      })
    });

    describe('detail', () => {
      beforeEach(() => {
        resource = client._resource({
          route: 'test',
          details: [
            { routeName: 'detail_get', method: 'get' }
          ]
        })
      })

      it('should get', () => {
        let result = { test: 'test' }
        nock(BASE)
          .get('/api/test/1/detail_get/')
          .reply(200, result)

        return resource.detail_get(1).then((res) => {
          assert.deepEqual(res, result)
        })
      })

      it('should get with params', () => {
        let result = { test: 'test' }
        nock(BASE)
          .get('/api/test/1/detail_get/?param1=1&param2=2')
          .reply(200, result)

        return resource.detail_get(1, { param1: 1, param2: 2 }).then((res) => {
          assert.deepEqual(res, result)
        })
      })
    })

    describe('list', () => {
      beforeEach(() => {
        resource = client._resource({
          route: 'test',
          lists: [
            { routeName: 'list_get', method: 'get' }
          ]
        })
      })

      it('should get', () => {
        let result = { test: 'test' }
        nock(BASE)
          .get('/api/test/list_get/')
          .reply(200, result)

        return resource.list_get().then((res) => {
          assert.deepEqual(res, result)
        })
      })

      it('should get with params', () => {
        let result = { test: 'test' }
        nock(BASE)
          .get('/api/test/list_get/?param1=1&param2=2')
          .reply(200, result)

        return resource.list_get({
          param1: 1,
          param2: 2,
        }).then((res) => {
          assert.deepEqual(res, result)
        })
      })

      it('should get with array params', () => {
        let result = { test: 'test' }
        nock(BASE)
          .get('/api/test/list_get/?param1=1&param2=2&param2=3')
          .reply(200, result)

        return resource.list_get({
          param1: 1,
          param2: [2, 3],
        }).then((res) => {
          assert.deepEqual(res, result)
        })
      })
    })

    describe('error', () => {
      it('should handle 400 bad request', () => {
        var res = [
          'error string',
          'error string',
        ]

        nock(BASE)
          .get('/api/test/1/')
          .reply(400, res)

        return resource.get(1).then((res) => {
          throw "this should have failed"
        }, (err) => {
          assert.deepEqual(err, res)
        })
      })
    })
  })

  describe('me', () => {
    it('should return the users\'s record', () => {
      nock(BASE)
        .get('/api/users/me/')
        .reply(200, {})

      return client.me()
    })
  })
})
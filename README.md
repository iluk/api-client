# Introduction

This API client will be used by the webapp and the mobile app to communicate with the server.

# Installation

This should always be installed using YARN (instead of NPM) via `yarn add git+ssh://git@gitlab.com/uluk/api-client.git`.

# Testing

Testing can be run via `npm test`.  Tests are contained in the `spec` directory.

# Releasing

A new release can be generated via `npm run release`.  This will do the following:

- Ask you what you want to tag the release as
- Update `package.json`
- Build the client
- Create a new commit containing the updated build & `package.json`
- Tag the commit with the release number
- Push the tags & commit to master

# Building

To build, run `npm run build`.  The build will be `dist/uluk-api-client.js`.  However, this shouldn't be necessary (you should only need to use `npm test` and `npm run release`).

# Client Builder (IMPORTANT)

You'll notice that `src/client.js` exports a `clientBuilder` function.  This is a little hack that allows us to inject the Node version of `fetch` (`node-fetch` package) while running dev tests so that a browser is not required.  In `src/index.js` you'll see that the `whatwg-fetch` package is used to polyfill `window.fetch` and to configure the client for the browser.